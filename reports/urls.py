from rest_framework.routers import DefaultRouter
from reports.views import reports_views

app_name = 'reports'

router = DefaultRouter()
router.register(
    'reports',
    reports_views.ReportsViewSet,
    basename='reports'
)
router.register(
    'report_files',
    reports_views.ReportFilesViewSet,
    basename='report_files'
)

urlpatterns = router.get_urls()
