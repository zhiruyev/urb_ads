from libs.randomize import get_random_string


def file_reports_upload(instance, filename):
    y = get_random_string(25)
    extension = filename.split(".")[-1]
    return f"reports/report_files/{y}.{extension}"
