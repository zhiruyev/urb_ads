from uuid import uuid4

from django.db import models
from django.utils.translation import gettext as _

from regions.models import (
    City,
    Area,
)
from reports.constants import FileType
from reports.utils.file_upload import file_reports_upload


class Report(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    user_id = models.IntegerField(null=True, blank=True)
    text = models.TextField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Жалоба')
        verbose_name_plural = _('Жалобы')


class ReportFile(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    report = models.ForeignKey(Report, blank=True, on_delete=models.CASCADE, related_name='files')
    type = models.PositiveSmallIntegerField(choices=FileType.choices(), default=FileType.TYPE_IMAGE)
    file = models.FileField(upload_to=file_reports_upload)

    class Meta:
        verbose_name = _('Файл жалобы')
        verbose_name_plural = _('Файлы жалобы')
