from django.contrib import admin

from reports.models import (
    Report,
    ReportFile,
)


class ReportFileInline(admin.TabularInline):
    model = ReportFile


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'city', 'area')
    search_fields = ('user_id',)
    list_filter = ('city', 'area')

    inlines = [
        ReportFileInline,
    ]
