from rest_framework import serializers

from reports.models import (
    Report,
    ReportFile,
)


class ReportCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = (
            'text',
            'city',
            'area',
            'user_id',
        )


class ReportFileCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportFile
        fields = (
            'report',
            'type',
            'file',
        )


class ReportFileListSerializer(serializers.ModelSerializer):

        class Meta:
            model = ReportFile
            fields = (
                'uuid',
                'type',
                'file',
            )


class ReportListSerializer(serializers.ModelSerializer):
    files = ReportFileListSerializer(many=True)

    class Meta:
        model = Report
        fields = (
            'uuid',
            'text',
            'city',
            'area',
            'files',
        )
