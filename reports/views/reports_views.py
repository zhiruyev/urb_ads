from rest_framework import viewsets, generics, permissions
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from libs.jwt_auth import UrbAuthentication
from reports.models import Report
from reports.serializers.reports_serializers import (
    ReportCreateSerializer,
    ReportFileCreateSerializer,
    ReportListSerializer,
)


class ReportsViewSet(
    viewsets.GenericViewSet,
    generics.CreateAPIView,
    generics.ListAPIView,
):
    queryset = Report.objects.all()
    authentication_classes = [UrbAuthentication]

    def get_permissions(self):
        return {
            'create': [permissions.IsAuthenticated()],
            'list': [permissions.IsAuthenticated()],
        }[self.action]

    def get_serializer_class(self):
        return {
            'create': ReportCreateSerializer,
            'list': ReportListSerializer,
        }[self.action]

    def perform_create(self, serializer):
        serializer.save(user_id=self.request.user.token['user_id'])

    def get_queryset(self):
        if self.action == 'list':
            return self.queryset.filter(user_id=self.request.user.token['user_id'])
        elif self.action == 'create':
            return self.queryset

        return self.queryset

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ReportFilesViewSet(
    viewsets.GenericViewSet,
    generics.CreateAPIView,
):
    authentication_classes = [UrbAuthentication]

    def get_permissions(self):
        return {
            'create': [permissions.IsAuthenticated()],
        }[self.action]

    def get_serializer_class(self):
        return {
            'create': ReportFileCreateSerializer,
        }[self.action]

    def perform_create(self, serializer):
        report = Report.objects.get(uuid=self.request.data['report'])
        if report.user_id != self.request.user.token['user_id']:
            raise ValidationError('You cannot add file to this report')

        super().perform_create(serializer)
