from django.db import models
from django.utils.translation import gettext as _


class City(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = _('Город')
        verbose_name_plural = _('Города')

    def __str__(self):
        return self.name


class Area(models.Model):
    name = models.CharField(max_length=100)
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='city')

    class Meta:
        verbose_name = _('Район')
        verbose_name_plural = _('Районы')

    def __str__(self):
        return self.name
