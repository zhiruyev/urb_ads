from rest_framework.routers import DefaultRouter
from regions.views import CitiesViewSet, AreasViewSet

app_name = 'regions'

router = DefaultRouter()
router.register(
    'cities',
    CitiesViewSet,
    basename='cities'
)
router.register(
    'areas',
    AreasViewSet,
    basename='areas'
)

urlpatterns = router.get_urls()
