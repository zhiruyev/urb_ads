from rest_framework import viewsets, generics
from rest_framework.response import Response

from regions.models import (
    City,
    Area,
)
from regions.serializers import (
    CityListSerializer,
    AreaListSerializer,
)


class CitiesViewSet(
    viewsets.GenericViewSet,
    generics.ListAPIView,
):
    queryset = City.objects.all()

    def get_serializer_class(self):
        return {
            'list': CityListSerializer,
        }[self.action]


class AreasViewSet(
    viewsets.GenericViewSet,
    generics.ListAPIView,
):
    queryset = Area.objects.all()

    def get_queryset(self):
        city_id = self.request.query_params.get('city_id', None)
        if city_id is not None:
            return self.queryset.filter(city_id=city_id)

        return self.queryset

    def get_serializer_class(self):
        return {
            'list': AreaListSerializer,
        }[self.action]

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
