from rest_framework import serializers

from regions.models import (
    City,
    Area,
)


class CityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name',)


class AreaListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = ('id', 'name',)
