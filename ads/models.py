from uuid import uuid4

import telebot
from django.db import models
from django.utils.translation import gettext as _
from django.conf import settings

from ads.constants import FileType
from ads.utils.file_upload import file_ads_upload
from regions.models import (
    City,
    Area,
)

bot = telebot.TeleBot(settings.BOT_TOKEN)


class AdvertisementType(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = _('Тип объявления')
        verbose_name_plural = _('Типы объявлений')

    def __str__(self):
        return self.name


class Organization(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    name = models.CharField(max_length=100)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = _('Организация')
        verbose_name_plural = _('Организации')

    def __str__(self):
        return self.name


class AdsBotChatids(models.Model):
    id = models.BigAutoField(primary_key=True)
    chat_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'ads_bot_chatids'


class Advertisement(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    title = models.CharField(max_length=100)
    ad_type = models.ForeignKey(AdvertisementType, on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    text = models.TextField()

    class Meta:
        verbose_name = _('Объявление')
        verbose_name_plural = _('Объявления')

    def save(self, *args, **kwargs):
        if settings.SENT_ADD_PUSHES:
            chat_ids = AdsBotChatids.objects.all()
            for chat in chat_ids:
                bot.send_message(
                    chat.chat_id,
                    f'Новое объявление: {self.title}\n'
                    f'{self.text}\n'
                    f'С уважением, {self.organization.name}'
                )

        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class AdvertisementFile(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, editable=False)
    advertisement = models.ForeignKey(Advertisement, blank=True, on_delete=models.CASCADE, related_name='files')
    type = models.PositiveSmallIntegerField(choices=FileType.choices(), default=FileType.TYPE_IMAGE)
    file = models.FileField(upload_to=file_ads_upload)

    class Meta:
        verbose_name = _('Файл события')
        verbose_name_plural = _('Файлы событий')
