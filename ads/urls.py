from rest_framework.routers import DefaultRouter
from ads.views import ads_views

app_name = 'registration'

router = DefaultRouter()
router.register(
    'ads',
    ads_views.AdvertisementViewSet,
    basename='ads'
)

urlpatterns = router.get_urls()
