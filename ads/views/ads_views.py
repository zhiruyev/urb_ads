from rest_framework import viewsets, generics

from ads.models import Advertisement
from ads.serializers.ads_serializers import (
    AdvertisementListSerializer,
    AdvertisementRetrieveSerializer,
)
from libs.pagination import StandardResultsSetPagination


class AdvertisementViewSet(
    viewsets.GenericViewSet,
    generics.ListAPIView,
    generics.RetrieveAPIView,
):
    pagination_class = StandardResultsSetPagination
    queryset = Advertisement.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return AdvertisementListSerializer

        return AdvertisementRetrieveSerializer
