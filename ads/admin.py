from django.contrib import admin

from ads.models import (
    AdvertisementFile,
    Advertisement,
    Organization,
    AdvertisementType,
)


class AdvertisementFileInline(admin.TabularInline):
    model = AdvertisementFile
    extra = 1


@admin.register(Advertisement)
class AdvertisementAdmin(admin.ModelAdmin):
    inlines = [AdvertisementFileInline]

    list_display = ('uuid', 'title', 'ad_type', 'organization')
    list_display_links = ('uuid', 'title')
    search_fields = ('uuid', 'title')


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'name')
    list_display_links = ('uuid', 'name')
    search_fields = ('uuid', 'name')


@admin.register(AdvertisementType)
class AdvertisementTypeAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'name')
    list_display_links = ('uuid', 'name')
    search_fields = ('uuid', 'name')
