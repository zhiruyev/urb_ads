from rest_framework import serializers

from ads.models import (
    Advertisement,
    Organization,
    AdvertisementFile,
    AdvertisementType,
)


class OrganizationSerializer(serializers.ModelSerializer):
    """Serializer for Organization model."""

    class Meta:
        model = Organization
        fields = ('uuid', 'name',)


class AdvertisementTypeSerializer(serializers.ModelSerializer):
    """Serializer for AdvertisementType model."""

    class Meta:
        model = AdvertisementType
        fields = ('uuid', 'name',)


class AdvertisementListSerializer(serializers.ModelSerializer):
    """Serializer for Advertisement model."""
    organization = OrganizationSerializer()
    ad_type = AdvertisementTypeSerializer()

    class Meta:
        model = Advertisement
        fields = ('uuid', 'title', 'ad_type', 'organization',)


class AdvertisementFileListSerializer(serializers.ModelSerializer):

    class Meta:
        model = AdvertisementFile
        fields = [
            'uuid',
            'type',
            'file'
        ]


class AdvertisementRetrieveSerializer(serializers.ModelSerializer):
    """Serializer for Advertisement model."""
    organization = OrganizationSerializer()
    ad_type = AdvertisementTypeSerializer()
    files = AdvertisementFileListSerializer(many=True)

    class Meta:
        model = Advertisement
        fields = ('uuid', 'title', 'text', 'ad_type', 'organization', 'files')
